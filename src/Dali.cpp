#include "Dali.h"

const int DALI_TX_PIN = 37;
const int DALI_RX_A_PIN = 38;

Dali::Dali() // constructor
{
	applyWorkAround1Mhz = 0;
}

void Dali::setTxPin(uint8_t pin)
{
	TxPin = pin; // user sets the digital pin as output
	pinMode(TxPin, OUTPUT);
	digitalWrite(TxPin, LOW);
}

void Dali::setRxAnalogPin(uint8_t pin)
{
	RxAnalogPin = pin; // user sets the digital pin as output
}

void Dali::workAround1MhzTinyCore(uint8_t a)
{
	applyWorkAround1Mhz = a;
}

void Dali::setupAnalogReceive(uint8_t pin)
{
	setRxAnalogPin(pin); // user sets the analog pin as input
}

void Dali::setupTransmit(uint8_t pin)
{
	setTxPin(pin);
	speedFactor = 2;
	// we don't use exact calculation of passed time spent outside of transmitter
	// because of high ovehead associated with it, instead we use this
	// emprirically determined values to compensate for the time loss

#if F_CPU == 1000000L				  // 1 Mhz
	uint16_t compensationFactor = 88; // must be divisible by 8 for workaround
#elif F_CPU == 8000000L				  // 8 Mhz
	uint16_t compensationFactor = 12; // Deze waarde werkt niet goed op atmega328PB external crystal. Die van 16 Mhz wel.
#elif F_CPU == 48000000UL			  // 48Mhz
	uint16_t compensationFactor = 2;
#else								  // 16Mhz
	uint16_t compensationFactor = 4; // Deze waarde lijkt goed te werken op 8 MHZ.
#endif

#if (F_CPU == 80000000UL) || (F_CPU == 160000000UL) // ESP8266 80MHz or 160 MHz
	delay1 = delay2 = (HALF_BIT_INTERVAL >> speedFactor) - 2;
#else
	delay1 = (HALF_BIT_INTERVAL >> speedFactor) - compensationFactor;
	delay2 = (HALF_BIT_INTERVAL >> speedFactor) - 2;
	period = delay1 + delay2;
#if F_CPU == 1
	delay2 -= 22; // 22+2 = 24 is divisible by 8
	if (applyWorkAround1Mhz)
	{ // definition of micro delay is broken for 1MHz speed in tiny cores as of now (May 2013)
		// this is a workaround that will allow us to transmit on 1Mhz
		// divide the wait time by 8
		delay1 >>= 3;
		delay2 >>= 3;
	}
#endif
#endif
}

uint8_t Dali::singleTransmit(uint8_t cmd1, uint8_t cmd2) // transmit commands to DALI bus (address byte, command byte)
{
	sendBit(1);
	sendByte(cmd1);
	sendByte(cmd2);
	digitalWrite(TxPin, LOW);
	return receive();
}

uint8_t Dali::transmit(uint8_t cmd1, uint8_t cmd2) // transmit commands to DALI bus (address byte, command byte)
{
	singleTransmit(cmd1, cmd2);
	return singleTransmit(cmd1, cmd2);
}

void Dali::sendByte(uint8_t b)
{
	for (int i = 7; i >= 0; i--)
	{
		sendBit((b >> i) & 1);
	}
}

void Dali::sendBit(int b)
{
	if (b)
		sendOne();
	else
		sendZero();
}

void Dali::sendZero(void)
{
	digitalWrite(TxPin, LOW);
	delayMicroseconds(delay2);
	digitalWrite(TxPin, HIGH);
	delayMicroseconds(delay1);
}

void Dali::sendOne(void)
{
	digitalWrite(TxPin, HIGH);
	delayMicroseconds(delay2);
	digitalWrite(TxPin, LOW);
	delayMicroseconds(delay1);
}

void Dali::splitAdd(long input, uint8_t &highbyte, uint8_t &middlebyte, uint8_t &lowbyte)
{
	highbyte = input >> 16;
	middlebyte = input >> 8;
	lowbyte = input;
}

int Dali::readBinaryString(char *s)
{
	int result = 0;
	while (*s)
	{
		result <<= 1;
		if (*s++ == '1')
			result |= 1;
	}
	return result;
}

uint8_t Dali::receive()
{

	unsigned long startFuncTime = 0;
	bool previousLogicLevel = 1;
	bool currentLogicLevel = 1;
	uint8_t arrLength = 20;
	int timeArray[arrLength];
	int i = 0;
	int k = 0;
	bool logicLevelArray[arrLength];
	int response = 0;

	dali.getResponse = false;

	startFuncTime = micros();

	// add check for micros overlap here!!!

	while (micros() - startFuncTime < dali.daliTimeout and i < arrLength)
	{
		// geting response
		if (digitalRead(dali.RxAnalogPin) > dali.analogLevel)
			currentLogicLevel = 1;
		else
			currentLogicLevel = 0;

		if (previousLogicLevel != currentLogicLevel)
		{
			timeArray[i] = micros() - startFuncTime;
			logicLevelArray[i] = currentLogicLevel;
			previousLogicLevel = currentLogicLevel;
			dali.getResponse = true;
			i++;
		}
	}

	arrLength = i;

	// decoding to manchester
	for (i = 0; i < arrLength - 1; i++)
	{
		if ((timeArray[i + 1] - timeArray[i]) > 0.75 * dali.period)
		{
			for (k = arrLength; k > i; k--)
			{
				timeArray[k] = timeArray[k - 1];
				logicLevelArray[k] = logicLevelArray[k - 1];
			}
			arrLength++;
			timeArray[i + 1] = (timeArray[i] + timeArray[i + 2]) / 2;
			logicLevelArray[i + 1] = logicLevelArray[i];
		}
	}

	k = 8;

	for (i = 1; i < arrLength; i++)
	{
		if (logicLevelArray[i] == 1)
		{
			if ((int)round((timeArray[i] - timeArray[0]) / (0.5 * dali.period)) & 1)
				response = response + (1 << k);
			k--;
		}
	}

	// remove start bit
	response = (uint8_t)response;
	return response;
}

//---------- PUBLIC ----------

void Dali::init()
{
	dali.setupTransmit(DALI_TX_PIN);
	dali.setupAnalogReceive(DALI_RX_A_PIN);
	dali.analogLevel = 444;
	dali.msgMode = true;
}

uint8_t Dali::sendCommand(uint8_t command){
	return transmit(BROADCAST_COMMAND, command);
}

uint8_t Dali::setLightLevel(uint8_t lightLevel)
{
	return transmit(BROADCAST_DIRECT, lightLevel);
}

uint8_t Dali::queryStatus()
{
	uint8_t status = transmit(BROADCAST_COMMAND, QUERY_STATUS);
	Serial1.print(F("Dali status: "));
	Serial1.println(status, HEX);

	// Ballast error
	if (status & 0b00000001)
		Serial1.println(F("Ballast error"));

	// Lamp burned out
	if (status & 0b00000010)
		Serial1.println(F("Lamp burned out"));

	// Ballast is on
	if (status & 0b00000100)
		Serial1.println(F("Ballast on"));

	// Lamp has been turned off and on again. Flag gets cleared when any intensity (dim) command is sent. (Power cycled since last intensity command)
	if (status & 0b10000000)
		Serial1.println(F("Lamp turn off/on"));

	return status;
}

Dali dali;
