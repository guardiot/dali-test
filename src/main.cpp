#include <Arduino.h>
#include "Dali.h"

unsigned long waitUntil = 0;
int maxLightLevel = 0;

/**
 * @brief Set up the variables that will be used for dimming
 */
void setup_dali()
{
  int minLightLevel = dali.sendCommand(GET_MIN_LIGHT_LEVEL);
  maxLightLevel = dali.sendCommand(GET_MAX_LIGHT_LEVEL);
  Serial1.print(F("Lightlevel min-max: "));
  Serial1.print(minLightLevel, HEX);
  Serial1.print(F("-"));
  Serial1.println(maxLightLevel, HEX);

  if (maxLightLevel == 0)
    maxLightLevel = 245;
}

void setup()
{
  while (!Serial1)
    ; // wait for Serial1 to be initialized
  Serial1.begin(9600);
  dali.init();
  setup_dali();
}

uint8_t flip = 0;

void loop()
{
  if (millis() > waitUntil)
  {
    dali.queryStatus();
    if (flip == 0){
      dali.setLightLevel(maxLightLevel);
      flip = 1;
      Serial1.print("Set to ");
      Serial1.println(maxLightLevel);
    } else {
      dali.setLightLevel(0);
      flip = 0;
      Serial1.println("Set to 0");
    }
    waitUntil = millis() + 5000;
  }
}