/*
  Copyright (c) 2014-2015 Arduino LLC.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "variant.h"

/*
 * Pins descriptions
 */
const PinDescription g_APinDescription[] = {
    // 0..13 - Digital pins
    // ----------------------
    // 0/1 - SERCOM/UART (Serial1)
    // 0
    {PORTA, 11, PIO_SERCOM, (PIN_ATTR_DIGITAL), No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_11}, // RX: SERCOM0/PAD[3]
                                     // 1
    {PORTA, 10, PIO_SERCOM, (PIN_ATTR_DIGITAL), No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_10}, // TX: SERCOM0/PAD[2]

    // 2..12
    // Digital Low
    // 2
    {PORTA, 14, PIO_SERCOM, (PIN_ATTR_DIGITAL), No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_14},
    // 3
    {PORTA, 9, PIO_SERCOM, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER),
     No_ADC_Channel, PWM0_CH1, TCC0_CH1, EXTERNAL_INT_9}, // TCC0/WO[1]
                                                          // 4
    {PORTA, 8, PIO_SERCOM, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER),
     No_ADC_Channel, PWM0_CH0, TCC0_CH0, EXTERNAL_INT_NMI}, // TCC0/WO[0]
                                                            // 5
    {PORTA, 15, PIO_DIGITAL, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER),
     No_ADC_Channel, PWM3_CH1, TC3_CH1, EXTERNAL_INT_15}, // TC3/WO[1]
                                                          // 6
    {PORTA, 20, PIO_TIMER_ALT, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER_ALT), No_ADC_Channel,
     PWM0_CH6, TCC0_CH6, EXTERNAL_INT_4}, // TCC0/WO[6]
                                          // 7
    {PORTA, 21, PIO_DIGITAL, (PIN_ATTR_DIGITAL), No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_5},

    // Digital High
    // 8
    {PORTA, 6, PIO_TIMER, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER),
     ADC_Channel6, PWM1_CH0, TCC1_CH0, EXTERNAL_INT_6}, // TCC1/WO[0]
                                                        // 9
    {PORTA, 7, PIO_TIMER, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER),
     ADC_Channel7, PWM1_CH1, TCC1_CH1, EXTERNAL_INT_7}, // TCC1/WO[1]
                                                        // 10
    {PORTA, 18, PIO_TIMER, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER),
     No_ADC_Channel, PWM3_CH0, TC3_CH0, EXTERNAL_INT_2}, // TC3/WO[0]
                                                         // 11
    {PORTA, 16, PIO_DIGITAL, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER),
     No_ADC_Channel, PWM2_CH0, TCC2_CH0, EXTERNAL_INT_0}, // TCC2/WO[0]
                                                          // 12
    {PORTA, 19, PIO_TIMER_ALT, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER_ALT),
     No_ADC_Channel, PWM0_CH3, TCC0_CH3, EXTERNAL_INT_3}, // TCC0/WO[3]

    // 13
    {PORTA, 17, PIO_DIGITAL, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER),
     No_ADC_Channel, PWM2_CH1, TCC2_CH1, EXTERNAL_INT_1}, // TCC2/WO[1]

    // 14..19 - Analog pins
    // --------------------
    // 14
    {PORTA, 2, PIO_ANALOG, PIN_ATTR_ANALOG, ADC_Channel0, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_2}, // ADC/AIN[0]
                                    // 15
    {PORTB, 8, PIO_SERCOM_ALT, (PIN_ATTR_PWM | PIN_ATTR_TIMER), ADC_Channel2,
     PWM4_CH0, TC4_CH0, EXTERNAL_INT_8},
    // 16
    {PORTB, 9, PIO_SERCOM_ALT, (PIN_ATTR_PWM | PIN_ATTR_TIMER), ADC_Channel3,
     PWM4_CH1, TC4_CH1, EXTERNAL_INT_9},
    // 17
    {PORTA, 4, PIO_ANALOG, 0, ADC_Channel4, NOT_ON_PWM, NOT_ON_TIMER,
     EXTERNAL_INT_4},
    // 18
    {PORTA, 5, PIO_ANALOG, 0, ADC_Channel5, NOT_ON_PWM, NOT_ON_TIMER,
     EXTERNAL_INT_5},
    // 19
    {PORTB, 2, PIO_ANALOG, 0, ADC_Channel10, NOT_ON_PWM, NOT_ON_TIMER,
     EXTERNAL_INT_2},

    // Extra Analog pins! 20..25
    // 20
    {PORTA, 11, PIO_ANALOG, (PIN_ATTR_DIGITAL | PIN_ATTR_ANALOG), ADC_Channel19,
     NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_11}, // A6 same as D0
                                                 // 21
    {PORTA, 20, PIO_ANALOG, (PIN_ATTR_DIGITAL | PIN_ATTR_ANALOG), ADC_Channel18,
     NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_10}, // A7 same as D1
                                                 // 22
    {PORTA, 27, PIO_ANALOG, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER | PIN_ATTR_ANALOG),
     ADC_Channel16, PWM0_CH0, TCC0_CH0, EXTERNAL_INT_NMI}, // A8 same as D4
                                                           // 23
    {PORTA, 5, PIO_ANALOG, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER | PIN_ATTR_ANALOG),
     ADC_Channel17, PWM0_CH1, TCC0_CH1, EXTERNAL_INT_9}, // A9 same as D3
                                                         // 24
    {PORTA, 6, PIO_ANALOG, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER | PIN_ATTR_ANALOG),
     ADC_Channel6, PWM1_CH0, TCC1_CH0, EXTERNAL_INT_6}, // A10 (unavailable)
                                                        // 25
    {PORTA, 7, PIO_ANALOG, (PIN_ATTR_DIGITAL | PIN_ATTR_PWM | PIN_ATTR_TIMER | PIN_ATTR_ANALOG),
     ADC_Channel7, PWM1_CH1, TCC1_CH1, EXTERNAL_INT_7}, // A11 same as D9

    // 26..27 I2C pins (SDA/SCL and also EDBG:SDA/SCL)
    // ----------------------
    // 26
    {PORTA, 22, PIO_SERCOM, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_6}, // SDA: SERCOM3/PAD[0]
                                    // 27
    {PORTA, 23, PIO_SERCOM, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_7}, // SCL: SERCOM3/PAD[1]

    // 28..30 - SPI pins (ICSP:MISO,SCK,MOSI)
    // ----------------------
    // 28
    {PORTA, 12, PIO_SERCOM, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_12}, // MOSI: SERCOM2/PAD[0]
                                     // 29
    {PORTB, 10, PIO_DIGITAL, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_10}, // !MOSI: SERCOM4/PAD[2] DEBUG TX
                                     // 30
    {PORTB, 11, PIO_DIGITAL, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_11}, // !SCK: SERCOM4/PAD[3]
    // 31
    {PORTA, 31, PIO_OUTPUT, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_NONE},
    // 32
    {PORTA, 28, PIO_OUTPUT, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_NONE},

    // 33..35
    // --------------------
    // 33
    {PORTA, 28, PIO_COM, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_NONE}, // USB Host enable
                                       // 34
    {PORTA, 24, PIO_DIGITAL, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_NONE}, // USB/DM
                                       // 35
    {PORTA, 25, PIO_COM, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_NONE}, // USB/DP

    // 36..38 - Secondary SPI
    // ----------------------
    // 36
    {PORTB, 03, PIO_SERCOM_ALT, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_NONE},
    // // 37 DALI TX
    // {PORTB, 22, PIO_SERCOM_ALT, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM,
    //  NOT_ON_TIMER, EXTERNAL_INT_NONE},
    // // 38 DALI RX
    // {PORTB, 23, PIO_SERCOM_ALT, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM,
    //  NOT_ON_TIMER, EXTERNAL_INT_NONE},
    // 37 DALI TX
    {PORTB, 22, PIO_DIGITAL, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_NONE},
    // 38 DALI RX
    {PORTB, 23, PIO_DIGITAL, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_NONE},
    // 39 Secondary SPI SS
    {PORTA, 27, PIO_DIGITAL, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_15},

    // 40
    {PORTA, 0, PIO_DIGITAL, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_NONE},
    // 41
    {PORTA, 1, PIO_DIGITAL, PIN_ATTR_NONE, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_NONE},

    // 42
    {PORTA, 3, PIO_ANALOG, PIN_ATTR_ANALOG, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_NONE},

    // 43
    {PORTA, 2, PIO_ANALOG, PIN_ATTR_ANALOG, DAC_Channel0, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_2},

    // 44 - SPI SERCOM2
    {PORTA, 13, PIO_SERCOM, PIN_ATTR_DIGITAL, No_ADC_Channel, NOT_ON_PWM,
     NOT_ON_TIMER, EXTERNAL_INT_12}, // SCK: SERCOM2/PAD[1]
};

const void *g_apTCInstances[TCC_INST_NUM + TC_INST_NUM] = {TCC0, TCC1, TCC2,
                                                           TC3, TC4, TC5};

// Multi-serial objects instantiation
SERCOM sercom0(SERCOM0);
SERCOM sercom1(SERCOM1);
SERCOM sercom2(SERCOM2);
SERCOM sercom3(SERCOM3);
SERCOM sercom4(SERCOM4);
//SERCOM sercom5(SERCOM5);

// Serial print
Uart Serial1(&sercom0, PIN_SERIAL1_RX, PIN_SERIAL1_TX, PAD_SERIAL1_RX,
             PAD_SERIAL1_TX);

// DALI
// Uart Serial2(&sercom5, PIN_SERIAL2_RX, PIN_SERIAL2_TX, PAD_SERIAL2_RX,
//              PAD_SERIAL2_TX);

// GPS
Uart Serial3(&sercom3, PIN_SERIAL3_RX, PIN_SERIAL3_TX, PAD_SERIAL3_RX,
             PAD_SERIAL3_TX);

void SERCOM0_Handler() { Serial1.IrqHandler(); }

//void SERCOM5_Handler() { Serial2.IrqHandler(); }

void SERCOM3_Handler() { Serial3.IrqHandler(); }
